//
//  AppDelegate.h
//  ScrollViewObjC
//
//  Created by Canyon Duncan on 8/31/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

