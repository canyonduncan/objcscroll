//
//  main.m
//  ScrollViewObjC
//
//  Created by Canyon Duncan on 8/31/17.
//  Copyright © 2017 Canyon Duncan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
